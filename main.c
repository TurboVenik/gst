#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include <string.h>
#include <sys/time.h>


//TODO короче в rank == 0 делаем инициализацию, дальше Bcast посылает M N Size может что-то еще..Затем scatter потом recv потом gather результат в rank == 0

//#include <gmpxx.h>

/// to compile: mpicc main.c -o main
/// to run:     mpirun main
/// to run with 4 threads: mpiexec --use-hwthread-cpus --n 4 ./main
/// to run with 2 threads: mpiexec -np 2 ./main

void writeln_to_file(FILE *f) {
    fwrite("\n", 1, 1, f);
}

void write_to_file(FILE *f, unsigned char simbol) {

    char str[4];
    snprintf(str, 4, "%u", simbol);
    fwrite(&str, strlen(str), 1, f);
    fwrite(" ", 1, 1, f);
}

void write_long_to_file(FILE *f, long simbol) {
    char str[11];
    snprintf(str, 11, "%li", simbol);
    fwrite(&str, strlen(str), 1, f);
}

void print_data_to_file(FILE *f, long matrix_count, unsigned char *data_set, long time) {

    fwrite("Размер данных: ", strlen("Размер данных: "), 1, f);
    write_long_to_file(f, matrix_count * 16 / 1024 / 1024);
    fwrite("Mb\n", 3, 1, f);

    fwrite("Время: ", strlen("Время: "), 1, f);
    write_long_to_file(f, time);
    fwrite("ms\n", 3, 1, f);

    for (long matrix_number = 0; matrix_number < matrix_count; matrix_number++)
        for (int raw_number = 0; raw_number < 4; raw_number++) {
            for (int column_number = 0; column_number < 4; column_number++)
                write_to_file(f, data_set[matrix_number * 16 + raw_number * 4 + column_number]);
            writeln_to_file(f);
        }
}

int f(int n) {

    if (n == 1)
        return n;
    return f(n - 1) * n;
}

void my_mix_column(unsigned char *r) {

    unsigned char a[4];
    unsigned char b[4];
    unsigned char c;
    int j;
    unsigned char h;

    for (c = 0; c < 4; c++) {
        a[c] = r[c];
        h = r[c] & 0x80;
        b[c] = r[c] << 1;
        if (h == 0x80)
            b[c] ^= 0x1b;
    }

//    f(1000);

    r[0] = b[0] ^ a[3] ^ a[2] ^ (b[1] ^ a[1]);
    r[1] = b[1] ^ a[0] ^ a[3] ^ (b[2] ^ a[2]);
    r[2] = b[2] ^ a[1] ^ a[0] ^ (b[3] ^ a[3]);
    r[3] = b[3] ^ a[2] ^ a[1] ^ (b[0] ^ a[0]);
}

void read_data(FILE *f, long matrix_count, unsigned char *data_set) {


    for (long matrix_number = 0; matrix_number < matrix_count; matrix_number++)
        for (int raw_number = 0; raw_number < 4; raw_number++)
            for (int column_number = 0; column_number < 4; column_number++) {
                fscanf(f, "%hhu", &data_set[matrix_number * 16 + raw_number * 4 + column_number]);
            }
}

void process_data(long matrix_count, unsigned char *data_set) {
    for (long i = 0; i < matrix_count; i++) {
        for (int j = 0; j < 4; j++) {
            my_mix_column(&data_set[i * 16 + j]);
        }
    }
}

long current_timestamp() {
    struct timeval te;
    gettimeofday(&te, NULL);
    long milliseconds = te.tv_sec * 1000LL + te.tv_usec / 1000;
    return milliseconds;
}

int main(int argc, char **argv) {

    // хрень какая то
    int rank, size, rc;
    long time;
    unsigned char *data_set = NULL;
    long matrix_count = 0;

    if ((rc = MPI_Init(&argc, &argv)) != MPI_SUCCESS) {

        fprintf(stderr, "Error starting MPI program. Terminating.\n");

        MPI_Abort(MPI_COMM_WORLD, rc);

    }
    unsigned char *recvbuf;
    /// Возвращает количество процессов в данном коммуникаторе
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    /// Возвращает наш ранк
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        // читка данных
        printf("READ DATA STARTS....\n");
        FILE *f = fopen("data500", "r");

        fscanf(f, "%li", &matrix_count);
        printf("%li матрицы\n", matrix_count);
        data_set = malloc(matrix_count * 4 * 4 * sizeof(unsigned char));

        // Чтение данных из файла
        read_data(f, matrix_count, data_set);
        printf("READ DATA ENDS....\n");
    }
    MPI_Bcast(&matrix_count, 1, MPI_INT, 0, MPI_COMM_WORLD);


    long part_count = matrix_count / size;

    //получить количество матриц
//    if (rank == 0)
    printf("process %d of %d всего матриц %ld  количество матриц на поток %ld\n", rank, size, matrix_count,
           part_count);

    if (rank == 0) {
        time = current_timestamp();
    }

    //получить данные от рута
    recvbuf = (unsigned char *) malloc(16 * sizeof(unsigned char) * part_count);
    MPI_Scatter(data_set, 16 * part_count, MPI_UNSIGNED_CHAR, recvbuf, 16 * part_count,
                MPI_UNSIGNED_CHAR, 0,
                MPI_COMM_WORLD);

    //что то сделать с данными
    process_data(part_count, recvbuf);


    MPI_Gather(recvbuf, 16 * part_count, MPI_UNSIGNED_CHAR, data_set, 16 * part_count,
               MPI_UNSIGNED_CHAR, 0,
               MPI_COMM_WORLD);

    if (rank == 0) {
        time = current_timestamp() - time;
        printf("time %li \n", time);
    }
    if (rank == 0) {
        FILE *f_out = fopen("data0_", "w");
        long time = current_timestamp();
//        print_data_to_file(f_out, matrix_count, data_set, time);
    }
    MPI_Finalize();

    return 0;
}
