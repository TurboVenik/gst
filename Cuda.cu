#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

//  nvcc Cuda.cu -o Cuda; ./Cuda

long current_timestamp() {
    struct timeval te;
    gettimeofday(&te, NULL);
    long milliseconds = te.tv_sec * 1000LL + te.tv_usec / 1000;
    return milliseconds;
}

void read_data(FILE *f, long matrix_count, unsigned char *data_set) {


    for (long matrix_number = 0; matrix_number < matrix_count; matrix_number++)
        for (int raw_number = 0; raw_number < 4; raw_number++)
            for (int column_number = 0; column_number < 4; column_number++) {
                fscanf(f, "%hhu", &data_set[matrix_number * 16 + raw_number * 4 + column_number]);
            }
}



__global__ void process(long grid_size, long treads, long column_count, unsigned char *data_set, int *answer) {

    long next_column = 0;

    while ((next_column + blockIdx.x * treads + threadIdx.x) <= column_count) {
//        if (blockIdx.x == 0)
//            atomicAdd(answer,1);
        unsigned char *r = &(data_set[4*next_column + 4*blockIdx.x * treads + threadIdx.x*4]);
        unsigned char a[4];
        unsigned char b[4];
        unsigned char c;
        unsigned char h;

        for (c = 0; c < 4; c++) {
            a[c] = r[c];
            h = r[c] & 0x80;
            b[c] = r[c] << 1;
            if (h == 0x80)
                b[c] ^= 0x1b;
        }

        r[0] = b[0] ^ a[3] ^ a[2] ^ (b[1] ^ a[1]);
        r[1] = b[1] ^ a[0] ^ a[3] ^ (b[2] ^ a[2]);
        r[2] = b[2] ^ a[1] ^ a[0] ^ (b[3] ^ a[3]);
        r[3] = b[3] ^ a[2] ^ a[1] ^ (b[0] ^ a[0]);


        next_column += grid_size * treads;
    }
}


int main(int argc, char *argv[]) {


    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    long matrix_count;
    unsigned char *data_set;
    unsigned char *dev_data_set;
    int size;

//    long t1, t2, t;
    long grid_size;
    long treads_size;

    grid_size = atoi(argv[2]);
    treads_size = atoi(argv[3]);

//    grid_size = 2147483647;
//    grid_size = 2;


    // читка данных
    printf("READ DATA STARTS....\n");
    FILE *f = fopen(argv[1], "r");

    fscanf(f, "%li", &matrix_count);
    printf("Количество матриц %li\n", matrix_count);
    printf("Количество столбцов %li\n", matrix_count * 4);
//    printf("Количество матриц на поток %li\n", matrix_count / N);

    size = matrix_count * 4 * 4 * sizeof(unsigned char);
    printf("Size %li\n", size);
    data_set = (unsigned char *) malloc(size);

    // Чтение данных из файла
    read_data(f, matrix_count, data_set);
    printf("READ DATA ENDS....\n");


//    t1 = current_timestamp();
    //выделяем память для device копий a, b, c


    cudaMalloc(&dev_data_set, size);


    // копируем ввод на device
    cudaEventRecord(start);
    cudaMemcpy(dev_data_set, data_set, size, cudaMemcpyHostToDevice);
//    t1 = current_timestamp() - t1;
//    printf("Copy on GPU time %li \n", t1);

    long column_count;


    column_count = matrix_count * 4;

//    t2 = current_timestamp();

//    t = current_timestamp();
    cudaEventRecord(start);
    long answer;


    int* d_answer;
    cudaMalloc(&d_answer, sizeof(int));


    process << < grid_size, treads_size >> > (grid_size, treads_size,  column_count, dev_data_set, d_answer);

    cudaEventRecord(stop);

//    cudaDeviceSynchronize();
//    t = current_timestamp() - t;
//    printf("GPU time %li \n", t);


    // копируем результат работы device обратно на host - копию c
    cudaMemcpy(data_set, dev_data_set, size, cudaMemcpyDeviceToHost);
    cudaEventSynchronize(stop);

    cudaMemcpy(&answer, d_answer, sizeof(int), cudaMemcpyDeviceToHost);

    printf("ANSWER %d", answer);
//    cudaMemcpyFromSymbol(&answer, "d_answer", sizeof(answer), 0, cudaMemcpyDeviceToHost);

    cudaFree(dev_data_set);
//    t2 = current_timestamp() - t2;
//    printf("Copy from GPU time %li \n", t2);

    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    printf("\n\nTIME %f\n\n", milliseconds);
//    printf("All time %li \n", t1 + t2);
    printf("AFTER\n\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%hhu %hhu %hhu %hhu \n", data_set[i * 16 + j * 4], data_set[i * 16 + j * 4 + 1],
                   data_set[i * 16 + j * 4 + 2], data_set[i * 16 + j * 4 + 3]);
        }
        printf("\n\n");
    }
    return 0;
}